***Settings***
Resource        base.robot
Test Setup      Nova sessão
Test Teardown   Encerra sessão

***Test Cases***
Selecionando por ID 
    [tags]      radio
    Go To                               ${url}/radios
    Select Radio Button                 movies             cap
    Radio Button Should Be Set to       movies             cap

Selecionando por Value 
    Go To                               ${url}/radios
    Select Radio Button                 movies             guardians  
    Radio Button Should Be Set to       movies             guardians
    Sleep                               3